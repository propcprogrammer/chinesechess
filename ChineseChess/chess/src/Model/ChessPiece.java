package Model;

public class ChessPiece {

	   private ChineseChess cc;
	   private DarkChess dc;
	   public ChessPiece(ChessBoard cb){
		   
		     if(cb instanceof ChineseChessBoard){
		    	  cc = new ChineseChess();
		     }
		     if(cb instanceof DarkChessBoard){
		    	  dc = new DarkChess();
		     }
		     
	   }
	   public ChineseChess getChineseChess(){
		   return this.cc;
	   }
	   public DarkChess getDarkChess(){
		   return this.dc;
	   }
}
