package Model;

import java.awt.Image;
import java.awt.Toolkit;

public class BlackChess {

	  private String name;
	  private String eng;
	  private int id;
	  private int x;
	  private int y;
	  private boolean live;
	  Image img;
	  
	  public BlackChess(String name,int id,int x,int y,boolean live,String imgSource){
		  
		   this.name = name;
		   this.id = id;
		   this.x = x;
		   this.y = y;
		   this.live = live;
		   this.img = Toolkit.getDefaultToolkit().getImage(getClass().getResource(imgSource+".png"));
		   
		   System.out.printf("%10s\t\t%d\t%d\t%d\n",this.name,this.id,this.x,this.y);
	  }
	  public void destroy(boolean die){
		  
		   this.live = die;
	  }
	  public boolean getStatus(){
		  return this.live;
	  }
	  public int getPositionX(){
		  return this.x;
	  }
	  public int getPositionY(){
		  return this.y;
	  }
	  public void setPosition(int x,int y){
		  this.x = x;
		  this.y = y;
	  }
	  public String getName(){
		  return this.name;
	  }
	  public void setEng(String eng){
		   this.eng = eng;
	  }
	  public String getEng(){
		   return this.eng;
	  }
	  public Image getBlackImage(){
		   return this.img;
	  }
	  public int getID(){
		   return this.id;
	  }
}
