package Model;

import java.util.Scanner;

public  class Operation {

	   Player player1,player2;
	   ChineseChess chess;
	   private String selectChessType;
	   private Rule rule;
	   private boolean select = false;
	   
	  public Operation(Player p1,Player p2,ChineseChess chess){
		  
		    this.player1 = p1;
		    this.player2 = p2;
		    this.chess = chess;
	  }
	  public void behavior(){
		   
		    while(true){
		    	
		    	 System.out.println("(1) operate (2) exit");
		    	 Scanner key = new Scanner(System.in);
		    	 int num = key.nextInt();
		    	  
		    	   if(num == 2){   break;    }
		    	   if(num == 1){
		    		   
		    		      if(player1.getRight()){
		    		    	  
		    		    	  System.out.println("player1   擁有棋權");
		    		    	  
		    		    	   if(player1.getOwner().equals("Red")){    System.out.println("Red");
		    		    		     
		    		    		     System.out.println("請選擇:");
		    		    		     
		    		    		     for(int i=0;i<(chess.getRedChess()).length;i++){
		    		    		    	  System.out.printf("[%d]%-10s\t",i, (chess.getRedChess())[i].getName());
		    		    		     }
		    		    		     selectChessType = "Red";
		    		    		     System.out.println();
		    		    	   }
		    		    	   if(player1.getOwner().equals("Black")){   System.out.println("Black");
		    		    		     
		    		    		     System.out.println("請選擇:");
		    		    		     
		    		    		     for(int i=0;i<(chess.getBlackChess()).length;i++){
		    		    		    	  System.out.printf("[%d]%-10s\t",i, (chess.getBlackChess())[i].getName());
		    		    		     }
		    		    		     selectChessType = "Black";
		    		    		     System.out.println();
		    		    	   }
		    		    	   
		    		    	   
		    		      }
		    		      if(player2.getRight()){
		    		    	   
		    		    	  System.out.println("player2   擁有棋權");
		    		    	  
		    		    	   if(player2.getOwner().equals("Red")){     System.out.println("Red");
		    		    		     
		    		    		     System.out.println("請選擇:");
		    		    		     
		    		    		     for(int i=0;i<(chess.getRedChess()).length;i++){
		    		    		    	  System.out.printf("[%d]%-10s\t",i, (chess.getRedChess())[i].getName());
		    		    		     }
		    		    		     selectChessType = "Red";
		    		    		     System.out.println();
		    		    	   }
		    		    	   if(player2.getOwner().equals("Black")){     System.out.println("Black");
		    		    		     
		    		    		     System.out.println("請選擇:");
		    		    		     
		    		    		     for(int i=0;i<(chess.getBlackChess()).length;i++){
		    		    		    	  System.out.printf("[%d]%-10s\t",i, (chess.getBlackChess())[i].getName());
		    		    		     }
		    		    		     selectChessType = "Black";
		    		    		     System.out.println();
		    		    	   }
		    		    	   
		    		      }
		    	   }// end of if
		    	 while(!select){
		    	    num = key.nextInt();
		    	      if(num>=0 && num<16){
		    	    	   rule = new Rule(selectChessType, num, chess);
		    	    	   
		    	    	   if(rule.select()){  this.select = true;   }
		    	    	   else{   this.select = false;    }
		    	    	 //  System.out.println("select =>"+select);
		    	      }
		    	      if(select){
		    	            if(selectChessType.equals("Red") || selectChessType.equals("Black")){
		    	    	   
		    	              while(true){
		    	    	           System.out.println("目的座標X:");
		    	    	            int x = key.nextInt();
		    	    	           System.out.println("目的座標Y:");
		    	    	            int y = key.nextInt();
		    	    	     
		    	    	          if( rule.movable(x, y) ){System.out.println("已走完");    break; }
		    	    	            System.out.println("重來");
		    	    	       }// end of while  
		    	    	   
		    	    	          if(!chess.getBlackChess()[0].getStatus()){   System.out.println("紅方勝利");    System.exit(1);   }
		    	    	          if(!chess.getRedChess()[0].getStatus()){   System.out.println("黑方勝利");    System.exit(1);   }
		    	    	   
		    	             }
		    	             else if(selectChessType == null){
		    	    	   
		    	    	              System.out.println("沒有選到棋子");
		    	              }
		    	      }
		    	 }//end of while 
		    	 select = false;
		    	   this.changeRight();
		    }// end of while
	  }
	  public void changeRight(){
		   
		    boolean right = player1.getRight();
		    player1.setRight(player2.getRight());
		    player2.setRight(right);
	  }
}
