package Model;

public class Rule {

	  private String selectChess;
	  private int index;
	  private ChineseChess chess;
	  private int fromX;
	  private int fromY;
	  private int toX;
	  private int toY;
	  
	  private RedChess fromRedChess;
	  private BlackChess fromBlackChess;
	
	    public Rule(String selectChess,int index,ChineseChess chess){
	    	
	    	  this.selectChess = selectChess;    System.out.println("*selectChess =>"+this.selectChess);
	    	  this.index = index;    System.out.println("*index =>"+this.index);
	    	  this.chess = chess;
	    } 	
	    public boolean select(){	 
	    	
	          if(selectChess.equals("Red") ){
	        	  System.out.println("boolean =>"+chess.getRedChess()[index].getStatus());
	        	    if( chess.getRedChess()[index].getStatus()){  
	    	     System.out.println("選擇了" + selectChess + chess.getRedChess()[index].getName() + "   ("+chess.getRedChess()[index].getPositionX()+","+chess.getRedChess()[index].getPositionY()+")");
	    	     
	    	            this.fromX = chess.getRedChess()[index].getPositionX();
	    	            this.fromY = chess.getRedChess()[index].getPositionY();
	    	       
	    	               fromRedChess = chess.getRedChess()[index];
	    	               fromBlackChess = null;
	    	               
	        	    }
	        	    else{System.out.println(chess.getRedChess()[index].getName()+"不存在");     return false;  }
	        	    
	        	     
  	          }
	          if(selectChess.equals("Black") ){
	        	    index = index - 17;
	        	  System.out.println("boolean =>"+chess.getBlackChess()[index].getStatus());
	        	    if( chess.getBlackChess()[index].getStatus()){
		    	     System.out.println("選擇了" + selectChess + chess.getBlackChess()[index].getName()+ "   ("+chess.getBlackChess()[index].getPositionX()+","+chess.getBlackChess()[index].getPositionY()+")");
		    	     
		    	         this.fromX = chess.getBlackChess()[index].getPositionX();
		    	         this.fromY = chess.getBlackChess()[index].getPositionY();
		    	       
		    	          fromRedChess = null;
		    	          fromBlackChess = chess.getBlackChess()[index];
	        	    }
	        	    else{System.out.println(chess.getBlackChess()[index].getName()+"不存在");    return false;   }
		      }
	          return true;
	    }
	    public boolean movable(int toX,int toY){
	    	
	    	  if(toX < 0){System.out.println("超出左邊界");    return false;   }
	    	  if(toX > 8){System.out.println("超出右邊界");    return false;   }
	    	  if(toY < 0){System.out.println("超出上邊界");    return false;   }
	    	  if(toY > 9){System.out.println("超出下邊界");    return false;   }
	    	  if(toX == fromX && toY == fromY){System.out.println("原始棋子");   return false;  }
	    	     this.toX = toX;
	    	     this.toY = toY;
	    	  if(!this.movable()){       return false;     }
	    	  System.out.println("正常");
	    	  return true;
	    }
	    public boolean movable(){
	    	  
	    	  if(selectChess.equals("Red")){
	    		    
	    		   System.out.println("fromX:"+fromX+"    "+"toX:"+toX);
	    		   System.out.println("fromY:"+fromY+"    "+"toY:"+toY);
	    		    if(chess.getRedChess()[index].getName().equals("General")){
	    		    	  System.out.println("RedGeneral:");    
	    		    	
	    		    	   if(toX < 3 || toX > 5 || toY > 2){
	    		    		   
	    		    		     if(toX == chess.getBlackChess()[0].getPositionX() && toY == chess.getBlackChess()[0].getPositionY()){}
	    		    		     else{System.out.println("超出邊界");    return false;   }
	    		    	   }
	    		    	   if((Math.abs(fromX - toX) + Math.abs(fromY - toY)) > 1){System.out.println("走法錯誤");    return false;   }
	    		    	   
	    		    	//   System.out.println("正常");
	    		    	
	    		    }//end of RedGeneral
	    		    else if(chess.getRedChess()[index].getName().equals("Advisor")){
	    		    	  System.out.println("RedAdvisor:");    
	    		    	
	    		    	   if(toX < 3 || toX > 5 || toY > 2){System.out.println("超出邊界");    return false;   }
	    		    	   if(Math.abs(fromX - toX) != 1 || Math.abs(fromY - toY) != 1){System.out.println("走法錯誤");    return false;   }
	    		    	//   System.out.println("正常");
	    		    	
	    		    }
	    		    else if(chess.getRedChess()[index].getName().equals("Elephant")){
	    		    	  System.out.println("RedElephant:");    
	    		    	
	    		    	   if(toY > 5){System.out.println("不能過河");    return false;   }
	    		    	   if(Math.abs(fromX - toX) != 2 || Math.abs(fromY - toY) != 2){System.out.println("走法錯誤");    return false;   }
	    		    	   if(chess.hasChess((fromX + toX)/2, (fromY + toY)/2)){System.out.println("象眼處有棋子");    return false;   }
	    		    	//   System.out.println("正常");
	    		    	
	    		    }
	    		    else if(chess.getRedChess()[index].getName().equals("Horse")){
	    		    	  System.out.println("RedHorse:");    
	    		    	      int i = 0;
	    		    	      int j = 0;
	    		    	   if( !((Math.abs(toX - fromX)==1 && Math.abs(toY - fromY)==2) || (Math.abs(toX - fromX)==2 && Math.abs(toY - fromY)==1)) ){System.out.println("馬不走日字型");    return false;   }
	    		    	   if(toX - fromX == 2){
	    		    		     i = fromX + 1;
	    		    		     j = fromY;
	    		    	   }
	    		    	   else if(fromX - toX == 2){
	    		    		     i = fromX - 1;
	    		    		     j = fromY;
	    		    	   }
	    		    	   else if(toY - fromY == 2){
	    		    		     i = fromX;
	    		    		     j = fromY + 1;
	    		    	   }
	    		    	   else if(fromY - toY == 2){
	    		    		     i = fromX;
	    		    		     j = fromY - 1;
	    		    	   }
	    		    	   if(chess.hasChess(i, j)){System.out.println("馬腳處有棋子");    return false;   }
	    		    	//   System.out.println("正常");
	    		    	
	    		    }
	    		    else if(chess.getRedChess()[index].getName().equals("Chariot")){
	    		    	  System.out.println("RedChariot:");    
	    		    	
	    		    	   if( fromX != toX && fromY != toY){System.out.println("非直線");    return false;   }
	    		    	   if(fromY == toY){
	    		    		   
	    		    		      if(fromX < toX){
	    		    		    	  
	    		    		    	    for(int i = fromX + 1;i<toX;i++){
	    		    		    	    	  if(chess.hasChess(i, fromY)){ System.out.println("有棋子");  return false;   }
	    		    		    	    }
	    		    		      }
	    		    		      else{
	    		    		    	    for(int i = fromX - 1;i>toX;i--){
	    		    		    	    	if(chess.hasChess(i, fromY)){ System.out.println("有棋子");  return false;   }
	    		    		    	    }
	    		    		      }
	    		    	   }
	    		    	   else{
	    		    		   
	    		    		     if(fromY < toY){
	    		    		    	  
   		    		    	            for(int i = fromY + 1;i<toY;i++){
   		    		    	    	          if(chess.hasChess(fromX,i)){ System.out.println("有棋子");  return false;   }
   		    		    	            }
   		    		              }
   		    		              else{
   		    		    	            for(int i = fromY - 1;i>toY;i--){
   		    		    	    	          if(chess.hasChess(fromX,i)){ System.out.println("有棋子");  return false;   }
   		    		    	            }
   		    		              }
	    		    	   }
	    		    	//   System.out.println("正常");
	    		    	
	    		    }
	    		    else if(chess.getRedChess()[index].getName().equals("Cannon")){
	    		    	  System.out.println("RedCannon:");    
	    		    	
	    		    	   if( fromX != toX && fromY != toY){System.out.println("非直線");    return false;   }
	    		    	   if(!chess.hasChess(toX, toY)){     System.out.println(" => 移動判斷");
	    		    		   
	    		    		     if(fromY == toY){   System.out.println(" => 橫向");
	    		    		    	 
	    		    		    	   if(fromX < toX){   System.out.println(" => 向右");
	    		    		    		   
	    		    		    		     for(int i=fromX + 1;i<toX;i++){
	    		    		    		    	 
	    		    		    		    	   if(chess.hasChess(i, fromY)){ System.out.println("有棋子");  return false;   }
	    		    		    		     }
	    		    		    	   }
	    		    		    	   else{    System.out.println(" => 向左");
	    		    		    		     for(int i=fromX - 1;i>toX;i--){
	    		    		    		    	 
	    		    		    		    	   if(chess.hasChess(i, fromY)){ System.out.println("有棋子");  return false;   }
	    		    		    		     }
	    		    		    	   }
	    		    		     }
	    		    		     else{    System.out.println(" => 縱向");
	    		    		    	 
	    		    		    	   if(fromY < toY){    System.out.println(" => 向後走");
	    		    		    		   
    		    		    		         for(int i=fromY + 1;i<toY;i++){
    		    		    		    	 
    		    		    		    	       if(chess.hasChess(fromX,i)){ System.out.println("有棋子");  return false;   }
    		    		    		         }
    		    		    	       }
    		    		    	       else{     System.out.println(" => 向前走");
    		    		    		         for(int i=fromY - 1;i>toY;i--){
    		    		    		        	 
    		    		    		    	       if(chess.hasChess(fromX,i)){ System.out.println("有棋子");  return false;   }
    		    		    		          }
    		    		    	       }
	    		    		     }
	    		    	   }
	    		    	   else{     System.out.println(" => 吃子判斷");
	    		    		   
	    		    		        int count = 0;
	    		    		   
	    		    		      if(fromY == toY){    System.out.println(" => 逢向");
	    		    		    	  
	    		    		    	      if(fromX < toX){    System.out.println(" => 向右走");
	    		    		    	    	   
	    		    		    	    	    for(int i = fromX + 1;i<toX;i++){
	    		    		    	    	    	
	    		    		    	    	    	if(chess.hasChess(i,fromY)){ System.out.println("有棋子"+chess.getBlackChess());   count++;   }
	    		    		    	    	    }
	    		    		    	    	    if(count != 1){   System.out.println("中間隔棋數錯誤");   return false;   }
	    		    		    	      }
	    		    		    	      else{    System.out.println(" => 向左走");
	    		    		    	    	    
	    		    		    	    	    for(int i = fromX - 1;i>toX;i--){
	    		    		    	    	    	
	    		    		    	    	    	if(chess.hasChess(i,fromY)){ System.out.println("有棋子");   count++;   }
	    		    		    	    	    }
	    		    		    	    	    if(count != 1){   System.out.println("中間隔棋數錯誤");   return false;   }
	    		    		    	      }
	    		    		      }
	    		    		      else{    System.out.println(" => 縱向");
	    		    		    	     if(fromY < toY){    System.out.println(" => 向後走");
	    		    		    	    	   
	    		    		    	    	    for(int i = fromY + 1;i<toY;i++){
	    		    		    	    	    	
	    		    		    	    	    	if(chess.hasChess(fromX,i)){ System.out.println("有棋子"+chess.getBlackChess());   count++;   }
	    		    		    	    	    }
	    		    		    	    	    if(count != 1){   System.out.println("中間隔棋數錯誤");   return false;   }
	    		    		    	     }
	    		    		    	     else{     System.out.println(" => 向前走");
	    		    		    	    	 
	    		    		    	    	    for(int i = fromY - 1;i>toY;i++){
	    		    		    	    	    	
	    		    		    	    	    	if(chess.hasChess(fromX,i)){ System.out.println("有棋子");   count++;   }
	    		    		    	    	    }
	    		    		    	    	    if(count != 1){   System.out.println("中間隔棋數錯誤");   return false;   }
	    		    		    	     }
	    		    		      }
	    		    	   }
	    		    }
	    		    else if(chess.getRedChess()[index].getName().equals("Soldier")){
	 	    		    	  System.out.println("RedSoldier:");    
	 	    		    	      
	 	    		    	    if(toY < fromY){ System.out.println("兵不能回頭");   return false;   }
	 	    		    	    if(fromY < 5 && fromY == toY){ System.out.println("過河前只能直走");   return false;   }
	 	    		    	    if( (Math.abs(toY - fromY) + Math.abs(toX - fromX)) > 1){ System.out.println("兵只能走一步");   return false;   }
	 	    		    //	   System.out.println("正常");
	 	    		    	
	 	    		    
	    		    	  
	    		    	   
	    		    	
	    		    }
	    		    
	    		    System.out.println("正常");
	    		    this.eatable();
	    		    
	    		      chess.getRedChess()[index].setPosition(toX, toY);
	    		    
	    	  }
	    	  if(selectChess.equals("Black")){
	    		    
	    //		   System.out.println("fromX:"+fromX+"    "+"toX:"+toX);
	    //		   System.out.println("fromY:"+fromY+"    "+"toY:"+toY);
	    		    if(chess.getBlackChess()[index].getName().equals("General")){
	    		    	  System.out.println("BlackGeneral:");    
	    		    	
	    		    	   if(toX < 3 || toX > 5 || toY < 7){
	    		    		   
	    		    		     if(toX == chess.getRedChess()[0].getPositionX() && toY == chess.getRedChess()[0].getPositionY()){}
	    		    		     else{System.out.println("超出邊界");    return false;   }
	    		    	   
	    		    	   }
	    		    	   if((Math.abs(fromX - toX) + Math.abs(fromY - toY)) > 1){System.out.println("走法錯誤");    return false;   }
	    		    //	   System.out.println("正常");
	    		    	
	    		    }
	    		    else if(chess.getBlackChess()[index].getName().equals("Advisor")){
	    		    	  System.out.println("BlackAdvisor:");    
	    		    	
	    		    	   if(toX < 3 || toX > 5 || toY < 7){System.out.println("超出邊界");    return false;   }
	    		    	   if(Math.abs(fromX - toX) != 1 || Math.abs(fromY - toY) != 1){System.out.println("走法錯誤");    return false;   }
	    		    //	   System.out.println("正常");
	    		    	
	    		    }
	    		    else if(chess.getBlackChess()[index].getName().equals("Elephant")){
	    		    	  System.out.println("BlackElephant:");    
	    		    	
	    		    	   if(toY < 5){System.out.println("不能過河");    return false;   }
	    		    	   if(Math.abs(fromX - toX) != 2 || Math.abs(fromY - toY) != 2){System.out.println("走法錯誤");    return false;   }
	    		    	   if(chess.hasChess((fromX + toX)/2, (fromY + toY)/2)){System.out.println("象眼處有棋子");    return false;   }
	    		    	//   System.out.println("正常");
	    		    	
	    		    }
	    		    else if(chess.getBlackChess()[index].getName().equals("Horse")){
	    		    	  System.out.println("BlackHorse:");    
	    		    	      int i = 0;
	    		    	      int j = 0;
	    		    	   if( !((Math.abs(toX - fromX)==1 && Math.abs(toY - fromY)==2) || (Math.abs(toX - fromX)==2 && Math.abs(toY - fromY)==1)) ){System.out.println("馬不走日字型");    return false;   }
	    		    	   if(toX - fromX == 2){
	    		    		     i = fromX + 1;
	    		    		     j = fromY;
	    		    	   }
	    		    	   else if(fromX - toX == 2){
	    		    		     i = fromX - 1;
	    		    		     j = fromY;
	    		    	   }
	    		    	   else if(toY - fromY == 2){
	    		    		     i = fromX;
	    		    		     j = fromY + 1;
	    		    	   }
	    		    	   else if(fromY - toY == 2){
	    		    		     i = fromX;
	    		    		     j = fromY - 1;
	    		    	   }
	    		    	   if(chess.hasChess(i, j)){System.out.println("馬腳處有棋子");    return false;   }
	    		    	//   System.out.println("正常");
	    		    	
	    		    }
	    		    else if(chess.getBlackChess()[index].getName().equals("Chariot")){
	    		    	  System.out.println("BlackChariot: before exam");    
	    		    	  
	    		    	 
	    		    	
	    		    	   if( fromX != toX && fromY != toY){System.out.println("非直線");    return false;   }
	    		    	   if(fromY == toY){      
	    		    		   
	    		    		      if(fromX < toX){     
	    		    		    	  
	    		    		    	    for(int i = fromX + 1;i<toX;i++){
	    		    		    	    	  if(chess.hasChess(i, fromY)){ System.out.println("有棋子");  return false;   }
	    		    		    	    }
	    		    		      }
	    		    		      else{      System.out.println("1-2");
	    		    		    	    for(int i = fromX - 1;i>toX;i--){
	    		    		    	    	if(chess.hasChess(i, fromY)){ System.out.println("有棋子");  return false;   }
	    		    		    	    }
	    		    		      }
	    		    	   }
	    		    	   else{      
	    		    		   
	    		    		     if(fromY < toY){   
	    		    		    	  
 		    		    	            for(int i = fromY + 1;i<toY;i++){
 		    		    	    	          if(chess.hasChess(fromX,i)){ System.out.println("有棋子");  return false;   }
 		    		    	            }
 		    		              }
 		    		              else{      
 		    		    	            for(int i = fromY - 1;i>toY;i--){
 		    		    	    	          if(chess.hasChess(fromX,i)){ System.out.println("有棋子");  return false;   }
 		    		    	            }
 		    		              }
	    		    	   }
	    		    	   System.out.println("BlackChariot: after exam");
	    		    	//   System.out.println("正常");
	    		    	
	    		    }
	    		    else if(chess.getBlackChess()[index].getName().equals("Cannon")){
	    		    	  System.out.println("BlackCannon:");    
	    		    	  System.out.println("fromX:"+fromX+"    "+"toX:"+toX);
		   	    		   System.out.println("fromY:"+fromY+"    "+"toY:"+toY);
		   	    		
	    		    	
	    		    	   if( fromX != toX && fromY != toY){System.out.println("非直線");    return false;   }
	    		    	   
	    		    	   if(chess.hasChess(toX, toY) == false){      System.out.println(" => 移動判斷");
	    		    		   
	    		    		     if(fromY == toY){  //橫向       
	    		    		    	 System.out.println(" => 橫向");
	    		    		    	   if(fromX < toX){  //向右走
	    		    		    		   System.out.println(" => 向右");
	    		    		    		     for(int i=fromX + 1;i<toX;i++){
	    		    		    		    	 
	    		    		    		    	   if(chess.hasChess(i, fromY) == true){ System.out.println("有棋子");  return false;   }
	    		    		    		     }
	    		    		    	   }
	    		    		    	   else{   //向左走
	    		    		    		   System.out.println(" => 向左");
	    		    		    		     for(int i=fromX - 1;i>toX;i--){
	    		    		    		    	 
	    		    		    		    	   if(chess.hasChess(i, fromY) == true){ System.out.println("有棋子");  return false;   }
	    		    		    		     }
	    		    		    	   }
	    		    		     }// end of if
	    		    		     else{  //縱向
	    		    		    	 System.out.println(" => 縱向");
	    		    		    	   if(fromY < toY){  //後面走
	    		    		    		   System.out.println(" => 向後走");
  		    		    		               for(int i=fromY + 1;i<toY;i++){
  		    		    		    	 
  		    		    		    	               if(chess.hasChess(fromX,i) == true){ System.out.println("有棋子");  return false;   }
  		    		    		               }
  		    		    	            }
  		    		    	            else{  //向前走
  		    		    	            	System.out.println(" => 向前走");
  		    		    	            	    for(int i=fromY - 1;i>toY;i--){
  		    		    		        	 
  		    		    		    	                if(chess.hasChess(fromX,i) == true){ System.out.println("有棋子");  return false;   }
  		    		    		                 }
  		    		    	             }
	    		    		      }// end of else
	    		    	   }// end of if
	    		    	   else{     // 吃子  
	    		    		   System.out.println(" => 吃子判斷");
	    		    		        int count = 0;
	    		    		   
	    		    		      if(fromY == toY){    //橫向
	    		    		    	  System.out.println(" => 逢向");
	    		    		    	      if(fromX < toX){  //向右走
	    		    		    	    	  System.out.println(" => 向右走");
	    		    		    	    	    for(int i = fromX + 1;i<toX;i++){
	    		    		    	    	    	
	    		    		    	    	    	if(chess.hasChess(i,fromY) == true){ System.out.println("有棋子");   count++;   }
	    		    		    	    	    }
	    		    		    	    	    if(count != 1){   System.out.println("中間隔棋數錯誤");   return false;   }
	    		    		    	      }
	    		    		    	      else{   //向左走
	    		    		    	    	  System.out.println(" => 向左走");
	    		    		    	    	    for(int i = fromX - 1;i>toX;i--){
	    		    		    	    	    	
	    		    		    	    	    	if(chess.hasChess(i,fromY) == true){ System.out.println("有棋子");   count++;   }
	    		    		    	    	    }
	    		    		    	    	    if(count != 1){   System.out.println("中間隔棋數錯誤");   return false;   }
	    		    		    	      }
	    		    		      }
	    		    		      else{   //縱向
	    		    		    	  System.out.println(" => 縱向");
	    		    		    	     if(fromY < toY){   //向後走
	    		    		    	    	 System.out.println(" => 向後走");
	    		    		    	    	    for(int i = fromY + 1;i<toY;i++){
	    		    		    	    	    	
	    		    		    	    	    	if(chess.hasChess(fromX,i) == true){ System.out.println("有棋子");   count++;   }
	    		    		    	    	    }
	    		    		    	    	    if(count != 1){   System.out.println("中間隔棋數錯誤");   return false;   }
	    		    		    	     }
	    		    		    	     else{   //向前走
	    		    		    	    	 System.out.println(" => 向前走");
	    		    		    	    	    for(int i = fromY - 1;i>toY;i--){
	    		    		    	    	    	
	    		    		    	    	    	if(chess.hasChess(fromX,i) == true){ System.out.println("有棋子"+chess.getBlackChess());   count++;   }
	    		    		    	    	    }
	    		    		    	    	    if(count != 1){   System.out.println("中間隔棋數錯誤");   return false;   }
	    		    		    	     }
	    		    		      }
	    		    	   }
	    		    	  
	    		    	   System.out.println("正常");
	    		    	
	    		    }
	    		    else if(chess.getBlackChess()[index].getName().equals("Soldier")){
	    		    	  System.out.println("BlackSoldier: before exam");   
	    		    	  
	    		    	
	    		    	      
	    		    	    if(toY > fromY){ System.out.println("兵不能回頭");   return false;   }
	    		    	    if(fromY > 4 && fromY == toY){ System.out.println("過河前只能直走");   return false;   }
	    		    	    if( (Math.abs(toY - fromY) + Math.abs(toX - fromX)) > 1){ System.out.println("兵只能走一步");   return false;   }
	    		    	    System.out.println("BlackSoldier: after exam"); 
	    		    //	   System.out.println("正常");
	    		    	
	    		    
  		    	  
  		    	   
  		    	
  		            }
	    		    System.out.println("正常");
	    		     this.eatable();
	    		     chess.getBlackChess()[index].setPosition(toX, toY);
	    		       
	    	  }
	    	  return true;
	    }// end of movable method
	    public void eatable(){
	    	
	    	  if(fromRedChess != null && fromBlackChess == null){
	    		  
	    		  System.out.println("fromRedChess");
	    		  
	    		      for(int i=0;i<10;i++){
	    		    	  
	    		    	     for(int j=0;j<9;j++){
	    		    	    	 
	    		    	    	     for(int k=0;k<chess.getBlackChess().length;k++){
	    		    	    	    	 
	    		    	    	    	   if(toX == chess.getBlackChess()[k].getPositionX() && toY == chess.getBlackChess()[k].getPositionY() ){
	    		    	    	    		     
	    		    	    	    		   System.out.println(chess.getBlackChess()[k].getName()+"被吃了_Black");
	    		    	    	    		      chess.getBlackChess()[k].destroy(false);
	    		    	    	    		      chess.getBlackChess()[k].setPosition(-1, -1);
	    		    	    	    		//      chess.getBlackChess()[k] = null;
	    		    	    	    	   }
	    		    	    	     }
	    		    	     }
	    		      }
	    	  }
	    	  if(fromRedChess == null && fromBlackChess != null){
	    		  
	    		  System.out.println("fromBlackChess");
	    		  
    		      for(int i=0;i<10;i++){
    		    	  
    		    	     for(int j=0;j<9;j++){
    		    	    	 
    		    	    	     for(int k=0;k<chess.getRedChess().length;k++){
    		    	    	    	 
    		    	    	    	   if(toX == chess.getRedChess()[k].getPositionX() && toY == chess.getRedChess()[k].getPositionY() ){
    		    	    	    		     
    		    	    	    		   System.out.println(chess.getRedChess()[k].getName()+"被吃了_Red");
    		    	    	    		      chess.getRedChess()[k].destroy(false);
    		    	    	    		      chess.getRedChess()[k].setPosition(-1, -1);
    		    	    	    		//      chess.getRedChess()[k] = null;
    		    	    	    	   }
    		    	    	     }
    		    	     }
    		      }
    	      }
	    	   
	    }
	   
}
