package Model;
import View.GameView;

public class ChineseChessBoard extends ChessBoard{

	   private ChessPiece piece;
	 public ChineseChessBoard(int x,int y,int w,int h,int spacex,int spacey){
		  
	//	     new GameView();
		  this.startx = x;
		  this.starty = y;
		  this.width = w;
		  this.height = h;
		  this.spacex = spacex;
		  this.spacey = spacey;
		  System.out.println("設置長寬完畢");
		  System.out.printf("起始X => %d  \t   起始Y => %d\n",this.startx,this.starty);
		  
		     for(int i=0;i<10;i++){
		    	 for(int j=0;j<9;j++){
		    		 
		    		  System.out.printf("( %d , %d )\t",j*spacex,i*spacey);
		    	 }
		    	 System.out.println();
		     }
		     System.out.println("============================================================================================");
		  piece = new ChessPiece(this);
		  
	  }
	  public void setStartPosition(int x,int y){
		  
		  this.startx = x;
		  this.starty = y;
		  System.out.println("設置起始位置完畢");
	  }
	  public int getStartPositionX(){
		  return this.startx;
	  }
	  public int getStartPositionY(){
		  return this.starty;
	  }
	  public ChessPiece getChess(){
		  return this.piece;
	  }
}
